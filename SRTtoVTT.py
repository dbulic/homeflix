import sys
import re
import codecs

# pri pokretanju skripte predati lokaciju srt filea (npr. "python3 SRTtoVTT.py /home/user/titlovi/titl1.srt")

def zamjeniZarez(dio):
    return re.sub(',', '.', dio.group(0))


srtFile = codecs.open(sys.argv[1], "r", "CP1252")

srt = srtFile.read()

vtt = re.sub(r'[0-9]{2},[0-9]{3}', zamjeniZarez, srt)

vtt = "WEBVTT\n\n" + vtt

vttFile = codecs.open(sys.argv[1].replace(
    ".srt", ".vtt"), "w", "utf16")

vttFile.writelines(vtt)
