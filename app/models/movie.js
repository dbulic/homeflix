/*
    Model za filmove - sadrzi metode za CRUD operacije nad filmovima u bazi
*/

const sql = require("../db.js");

// constructor
const Movie = function(movie) {
    this.name = movie.name;
    this.description = movie.description;
    this.img = movie.img;
    this.filepath = movie.filepath;
};

Movie.create = (newMovie, callback) => {
    sql.query("INSERT INTO movies SET ?", newMovie, (err, res) => {
        if (err) {
            console.log("Error: " + err);
            callback(err, null);
            return;
        }
        // console.log("Created movie: ", {
        //     id: res.insertId,
        //     ...newMovie
        // });
        callback(null, {
            id: res.insertId,
            ...newMovie,
        });
    });
};

Movie.getByFav = (user_id, callback) => {
    sql.query(
        "SELECT * FROM movies WHERE id IN (SELECT movie_id FROM favorites WHERE user_id=?)",
        user_id,
        (err, res) => {
            if (err) {
                console.log(err);
                callback(err, null);
                return;
            }
            callback(null, JSON.parse(JSON.stringify(res)));
        }
    );
};

Movie.getAll = (where, callback) => {
    // console.log(where)
    if (!where || where.trim() == "") {
        sql.query("SELECT * FROM movies", (err, res) => {
            if (err) {
                console.log("error: ", err);
                callback(err, null);
                return;
            }
            // console.log("movies: ", JSON.parse(JSON.stringify(res)));
            callback(null, JSON.parse(JSON.stringify(res)));
        });
    } else {
        sql.query(
            "SELECT * FROM movies WHERE lower(name) LIKE ?", ["%" + where.toLowerCase() + "%"],
            (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    callback(err, null);
                    return;
                }
                // console.log("movies: ", JSON.parse(JSON.stringify(res)));
                callback(null, JSON.parse(JSON.stringify(res)));
            }
        );
    }
};

Movie.findById = (id, callback) => {
    sql.query("SELECT * FROM movies WHERE id=?", [id], (err, res) => {
        if (err) {
            console.log("error: ", err);
            callback(err, null);
            return;
        }
        // console.log("movies: ", JSON.parse(JSON.stringify(res)));
        callback(null, JSON.parse(JSON.stringify(res)));
    });
};

Movie.destroy = (id, callback) => {
    sql.query("DELETE FROM movies WHERE id=?", [id], (err, res) => {
        if (err) {
            console.log("error: ", err);
            callback(err, null);
            return;
        }
        // console.log("movies: ", JSON.parse(JSON.stringify(res)));
        callback(null, JSON.parse(JSON.stringify(res)));
    });
};

module.exports = Movie;