const sql = require("../db");

const Subtitle = function(subtitle) {
    this.movie_id = subtitle.movie_id;
    this.filepath = subtitle.filepath;
    this.lang = subtitle.lang;
    this.langAbbr = subtitle.langAbbr;
};

Subtitle.create = (newSub, callback) => {
    sql.query("INSERT INTO subtitles SET ?", newSub, (err, res) => {
        if (err) {
            console.log(err);
            callback(err, null);
            return;
        }
        callback(null, { id: res.insertId, ...newSub });
    });
};

Subtitle.findById = (id, callback) => {
    sql.query("SELECT * FROM subtitles WHERE movie_id=?", [id], (err, res) => {
        if (err) {
            console.log(err);
            callback(err, null);
            return;
        }
        callback(null, JSON.parse(JSON.stringify(res)));
    });
};

module.exports = Subtitle;