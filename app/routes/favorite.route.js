const express = require("express");
const router = express.Router();
const Favorite = require("../models/favorite");

router.get("/", (req, res) => {
    Favorite.find([req.query.user_id, req.query.movie_id], (err, data) => {
        if (!err) {
            console.log(data);
            res.status(200).send(JSON.stringify(data));
        }
    });
});

router.post("/", (req, res) => {
    Favorite.create({
            user_id: req.body.user_id,
            movie_id: req.body.movie_id,
        },
        (err, data) => {
            if (!err) {
                res.status(201).json({ message: "Uspjesno dodan u favorite" });
            }
        }
    );
});

router.post("/delete", (req, res) => {
    Favorite.find([req.body.user_id, req.body.movie_id], (err, data) => {
        if (err) {
            res.status(500).json({ message: "Krivi zahtjev!" });
            return;
        }
        console.log(data);
        Favorite.destroy(data[0].id, (err, data) => {
            if (!err) {
                res.status(200).json({
                    message: "Uspješno ukonjen sa favorita",
                });
            }
        });
    });
});

module.exports = router;