/*
    Router za /movies rutu - za odredjene GET i POST requestove

    nisam radio controller za movies pa je logika iz controllera
    napisana ovdje u samim rutama
*/

const express = require("express");
const router = express.Router();
const Movie = require("../models/movie");
const B64 = require("base64-img");
const fs = require("fs");
const path = require("path");

router.get("/", (req, res) => {
    // ruta /movies

    // dohvaca sve filmove ako nije postavljen get parametar "where"
    // ako je postavljen onda vraca filmove koji u nazivu imaju kljucnu rijec
    // iz parametra "where"

    // console.log(req.query.where)

    Movie.getAll(req.query.where, (err, data) => {
        if (err) {
            res.status(500).json(err);
        } else {
            // console.log(data)
            res.status(200).send(JSON.stringify(data));
        }
    });

    // res.status(200).send(
    //     JSON.stringify({
    //         filmovi: [{
    //                 id: 1,
    //                 name: 'Be carefull BB8',
    //                 img: 'bb8.jpg',
    //             },
    //             {
    //                 id: 2,
    //                 name: 'Maze runner',
    //                 img: 'maze1.jpg',
    //             },
    //         ],
    //     })
    // );
});

router.get("/:id", (req, res) => {
    // ruta /movies/[id] dohvaca video stream filma sa odgovarajucim id

    Movie.findById(req.params.id, (err, movie) => {
        //dohvacamo film pomocu id
        if (!err) {
            try {
                // console.log(movie)
                const filePath = movie[0].filepath; // koristimo lokaciju filma (iz baze) za streamanje
                const stat = fs.statSync(filePath);
                const fileSize = stat.size;
                const range = req.headers.range;

                // definiramo raspon djelova koji se salju
                if (range) {
                    const parts = range.replace(/bytes=/, "").split("-");
                    const start = parseInt(parts[0], 10);
                    const end = parts[1] ?
                        parseInt(parts[1], 10) :
                        fileSize - 1;

                    if (start >= fileSize) {
                        res.status(416).send(
                            "Requst range not satisfiable: " +
                            start +
                            " >= " +
                            fileSize
                        );
                        return;
                    }

                    const chunkSize = end - start + 1;
                    const file = fs.createReadStream(filePath, {
                        start,
                        end,
                    });
                    const head = {
                        "Content-Range": `bytes ${start}-${end}/${fileSize}`,
                        "Accept-Ranges": "bytes",
                        "Content-Length": chunkSize,
                        "Content-Type": "video/mp4",
                    };
                    res.writeHead(206, head); // postavljamo headere sa statusom 206 (nedovrseni podatci)
                    fs.createReadStream(filePath);
                    file.pipe(res); // saljemo djelove dio po dio
                } else {
                    // ako nije odredjen raspon posalji cijeli sadrzaj
                    const head = {
                        "Content-Length": fileSize,
                        "Content-Type": "video/mp4",
                    };
                    res.writeHead(200, head);
                    fs.createReadStream(filePath).pipe(res);
                }
            } catch {
                res.status(404).json({
                    message: "Nije pronađen ovaj film na disku! Moguće je da je premješten na drugu lokaciju!",
                });
            }
        }
    });
});

router.get("/favorite/:user", (req, res) => {
    Movie.getByFav(req.params.user, (err, data) => {
        if (err) {
            res.status(500).json(err);
        } else {
            console.log(data);
            res.status(200).send(JSON.stringify(data));
        }
    });
});

router.post("/", (req, res) => {
    // ruta /movies [POST]

    // u request bodyju prima naziv, opis, poster, i lokaciju filma
    // poster je u base64 string obliku te se on sprema u /src/assets direktorij
    // ostale informacije kao i put do (novo spremljenog) postera se spremaju u
    // tablicu movies u bazi
    // metoda vraca kreirani JSON objekt (sa id-jem iz baze) natrag na frontend

    // console.log(req.body)

    B64.img(
        req.body.img,
        "src/assets",
        req.body.name.replace(/ /g, "-"),
        function(err, imgfilepath) {
            if (!err) {
                Movie.create({
                        name: req.body.name,
                        description: req.body.description,
                        img: imgfilepath,
                        filepath: req.body.filepath,
                    },
                    (err, data) => {
                        if (!err) {
                            res.status(201).json({
                                message: "Uspjesno dodan film",
                            });
                        }
                    }
                );
            }
        }
    );
});

router.delete("/:id", (req, res) => {
    console.log("del req id:" + req.params.id);
    Movie.findById(req.params.id, (err, data) => {
        if (!err) {
            // console.log(path.join(__dirname, "../../", data[0].img))
            // return;
            fs.unlinkSync(path.join(__dirname, "../../", data[0].img));

            Movie.destroy(req.params.id, (err, smt) => {
                if (!err) {
                    res.status(200).json({
                        message: "Uspješno izbrisan film!",
                    });
                } else {
                    console.log(err);
                }
            });
        }
    });
});

module.exports = router;