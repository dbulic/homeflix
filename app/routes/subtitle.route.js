const express = require("express");
const router = express.Router();
const fs = require("fs");
const Subtitle = require("../models/subtitle");

router.get("/:id", (req, res) => {
    Subtitle.findById(req.params.id, (err, data) => {
        if (err) {
            return;
        }
        res.status(200).json(data);
    });
});

router.post("/", (req, res) => {
    let path =
        "src/assets/subtitles/" +
        req.body.movie_id +
        "-" +
        req.body.langAbbr +
        ".vtt";
    fs.writeFileSync(path, req.body.file);
    Subtitle.create({
            movie_id: req.body.movie_id,
            filepath: path,
            lang: req.body.lang,
            langAbbr: req.body.langAbbr,
        },
        (err, data) => {
            if (!err) {
                res.status(201).json({ message: "Uspjesno dodan podnaslov" });
            }
        }
    );
});
module.exports = router;