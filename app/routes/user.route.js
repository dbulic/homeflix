/*
    Router za /users rutu - za odredjene GET i POST requestove

    nisam radio controller za movies pa je logika iz controllera
    napisana ovdje u samim rutama
*/

const express = require("express");
const router = express.Router();
const User = require("../models/user");

router.get("/", (req, res) => {
    // ruta /users

    // dohvaca sve korisnike

    User.getAll(null, (err, data) => {
        if (err) {
            res.status(500).json(err);
        } else {
            // console.log(data)
            res.status(200).send(JSON.stringify(data));
        }
    });
});

router.get("/:id", (req, res) => {
    // ruta /users/[id] dohvaca video stream filma sa odgovarajucim id

    User.findById(req.params.id, (err, user) => {
        //dohvacamo korisnika pomocu id
        if (!err) {
            res.status(200).send(JSON.stringify(user));
        } else {
            res.status(500).send(JSON.stringify(err));
        }
    });
});

router.post("/", (req, res) => {
    // ruta /users [POST]

    // u request bodyju prima ime korisnika
    // podatci se spremaju u tablicu users
    // metoda vraca kreirani JSON objekt (sa id-jem iz baze) natrag na frontend

    // console.log(req.body)

    User.create({
            name: req.body.name,
        },
        (err, data) => {
            if (err) return res.status(500).send(JSON.stringify(err));
            res.status(200).send(JSON.stringify(data));
        }
    );
});

router.delete("/:id", (req, res) => {
    User.destroy(req.params.id, (err, data) => {
        if (err) return res.status(500).send(JSON.stringify(err));
        res.status(200).send(
            JSON.stringify({ message: "Uspjesno izbrisan korisnik!" })
        );
    });
});

module.exports = router;