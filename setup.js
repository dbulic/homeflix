// skripta koja kreira tablice u bazi

const con = require("./app/db");
require("dotenv").config();

var sql =
    "CREATE TABLE `movies` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , `description` VARCHAR(255) NOT NULL , `img` VARCHAR(255) NOT NULL , `filepath` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB; ";
sql +=
    "CREATE TABLE `favorites` ( `id` INT NOT NULL AUTO_INCREMENT , `user_id` INT NOT NULL , `movie_id` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB; ";
sql +=
    "CREATE TABLE `users` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB; ";
sql +=
    "CREATE TABLE `subtitles` ( `id` INT NOT NULL AUTO_INCREMENT , `movie_id` INT NOT NULL , `filepath` VARCHAR(255) NOT NULL , `lang` VARCHAR(255) NOT NULL , `langAbbr` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB; ";

con.query(sql, function(err, result, fields) {
    if (err) throw err;
    console.log(result);
    con.end();
    process.exit();
});